(function () {
    angular
        .module("PPPApp")
        .controller("PPPController", PPPController)
        .controller("PPPCloseModalController", PPPCloseModalController);


        PPPController.$inject = ['$uibModal', '$state'];
        PPPCloseModalController.$inject = ['$uibModalInstance','$state'];
    
    function PPPController($uibModal, $state) {
       
        var vm = this;
        vm.format = "EEEE, MMMM d, y h:mm:ss a";
      
        vm.user = [];

        vm.initApp = initApp;
        vm.registerForm = registerForm;
        vm.registerApp = registerApp;
        vm.appLogin = appLogin;

        function initApp(){
            var vm = this;

            vm.pwcheck = 0;
           

            console.log("init >" + vm.pwcheck);

        }

       

        function appLogin(size,parentSelector){
            console.log("------Start registerApp-----");
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './loginform.html',
                controller: 'PPPCloseModalController',
                controllerAs: 'ctrl',
                size: 'lg',
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return size;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function registerForm(){
            console.log("------load registerForm-----");
            $state.go('register');
        }
       
        function registerApp(size,parentSelector){
            console.log("------Start registerApp-----");
            console.log(vm.user.serviceprovider);
            console.log(vm.user.mobileno);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './registerOK.html',
                controller: 'PPPCloseModalController',
                controllerAs: 'ctrl',
                size: 'lg',
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return size;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        initApp();
    
    }// end of PPPController()
        
    function PPPCloseModalController($uibModalInstance, $state){
        var vm = this;
        vm.profileMain = profileMain;
        vm.checkApppw = checkApppw;
        vm.loginFailed = loginFailed;

        vm.appLockpw = '111';
        vm.pwcheck = 0;
        
        

        function profileMain(){
            console.log("------load profileMain-----");
            $uibModalInstance.close(self.run);
            $state.go('profile');

        }

        function checkApppw(){

            console.log("------load checkApppw-----");
  
            if (vm.user.pw == vm.appLockpw) {
               console.log("pw > " + vm.user.pw);
               console.log("appLockpw > " + vm.appLockpw);
                $uibModalInstance.close(self.run);
                $state.go('profile');
            } else {
            console.log("------password wrong-----");               
                vm.pwcheck = 1;

            }

        }

        function loginFailed(){
        console.log("------load loginFailed-----");
            $uibModalInstance.close(self.run);
            $state.go('load');
        }


    }   // end of PPPCloseModalController()



    
})();


/*


        .controller("EditEmployeeCtrl", EditEmployeeCtrl)
        .controller("AddEmployeeCtrl", AddEmployeeCtrl)
        .controller("DeleteEmployeeCtrl", DeleteEmployeeCtrl);

    EditEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    AddEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    DeleteEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];

 function EmployeeController(EMSAppAPI, $uibModal, $document, $scope) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.employees = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 10;
        self.currentPage = 1;

        self.searchEmployees =  searchEmployees;
        self.addEmployee =  addEmployee;
        self.editEmployee = editEmployee;
        self.deleteEmployee = deleteEmployee;
        self.pageChanged = pageChanged;

        function searchAllEmployees(searchKeyword,orderby,itemsPerPage,currentPage){
            EMSAppAPI.searchEmployees(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.employees = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllEmployees(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshEmployeeList",function(){
            console.log("refresh employee list "+ self.searchKeyword);
            searchAllEmployees(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshEmployeeListFromAdd",function(event, args){
            console.log("refresh employee list from emp_no"+ args.emp_no);
            var employees = [];
            employees.push(args);
            self.searchKeyword = "";
            self.employees = employees;
        });

        function searchEmployees(){
            console.log("search employees  ....");
            console.log(self.orderby);
            searchAllEmployees(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

       

        

       
    }



    function DeleteEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteEmployee = deleteEmployee;
        console.log(items);
        EMSAppAPI.getEmployee(items).then((result)=>{
            console.log(result.data);
            self.employee =  result.data;
            self.employee.birth_date = new Date( self.employee.birth_date);
            self.employee.hire_date = new Date( self.employee.hire_date);
            console.log(self.employee.birth_date);
        });

        function deleteEmployee(){
            console.log("delete employee ...");
            EMSAppAPI.deleteEmployee(self.employee.emp_no).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEmployeeList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Add Employee");
        var self = this;
        self.saveEmployee = saveEmployee;

        self.employee = {
            gender: "M"
        }
        initializeCalendar($scope);
        function saveEmployee(){
            console.log("save employee ...");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.hire_date);
            console.log(self.employee.birth_date);
            console.log(self.employee.gender);
            EMSAppAPI.addEmployee(self.employee).then((result)=>{
                //console.log(result);
                console.log("Add employee -> " + result.emp_no);
                $rootScope.$broadcast('refreshEmployeeListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }
    }




function initializeCalendar($scope){
        self.datePattern = /^\d{4}-\d{2}-\d{2}$/;;
        
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
    
        $scope.clear = function() {
            $scope.dt = null;
        };
    
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
    
        $scope.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            console.log(data);
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    
        $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
    
        $scope.toggleMin();
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
    
        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
    
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        $scope.altInputFormats = ['M!/d!/yyyy'];
    
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
        ];
    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
        
                for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
                }
            }
        
            return '';
        }
    }





    function EditEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Employee Ctrl");
        var self = this;
        self.items = items;
        initializeCalendar($scope);

        EMSAppAPI.getEmployee(items).then((result)=>{
           console.log(result.data);
           self.employee =  result.data;
           self.employee.birth_date = new Date( self.employee.birth_date);
           self.employee.hire_date = new Date( self.employee.hire_date);
           console.log(self.employee.birth_date);
        })

        self.saveEmployee = saveEmployee;

        function saveEmployee(){
            console.log("save employee ...");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.hire_date);
            console.log(self.employee.birth_date);
            console.log(self.employee.gender);
            EMSAppAPI.updateEmployee(self.employee).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEmployeeList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }






 function addEmployee(size, parentSelector){
            console.log("post add employee  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addEmployee.html',
                controller: 'AddEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

function editEmployee(emp_no, size, parentSelector){
            console.log("Edit Employee...");
            console.log("emp_no > " + emp_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/editEmployee.html',
                controller: 'EditEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return emp_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }


 function deleteEmployee(emp_no, size, parentSelector){
            console.log("delete Employee...");
            console.log("emp_no > " + emp_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteEmployee.html',
                controller: 'DeleteEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return emp_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }


*/