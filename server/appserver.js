var express = require("express");
var bodyParser = require('body-parser');
var Sequelize = require ("sequelize");

var node_port = process.env.PORT || 3000;


//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="1111"
var connection = new Sequelize(
    'project1',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3308,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);



//var Employees = require('./models/employees')(connection, Sequelize);
var Class = require('./models/class')(connection, Sequelize);


var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(__dirname + "/../client/"));








app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
});

module.exports = app;

/*

Class.findOne().then(Class => {
    console.log(Class.get('class'));
  });

Class.findAll().then(Class => {
    console.log(Class)
  });


connection
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});

*/