module.exports = function(connection, Sequelize){

    var Class = connection.define('class', {
        claID: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        orgID: {
            type: Sequelize.INTEGER(11),
            allowNull: false
        },
        class: {
            type: Sequelize.STRING,
            allowNull: false
        },
        year:{
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        freezeTableName: true, 
        timestamps: false
    });
    return Class;
}