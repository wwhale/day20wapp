module.exports = function(connection, Sequelize){
    // employees this have to match the mysql table
    // return Employees object is used within the JS
    var Employees = connection.define('employees', {
        emp_no: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        gender:{
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        hire_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        photourl: {
            type: Sequelize.STRING,
            defaultValue: "https://upload.wikimedia.org/wikipedia/commons/d/de/Facebook_head.png"
        }
    }, {
        timestamps: false
    });
    return Employees;
}